fetch("http://localhost:4000/courses")
.then(res => res.json())
.then(data => {
	// console.log(data[0].name)
// Activity
// display a single course name in the header of the HTML via our live data (not hard coded)
let header = document.querySelector("header")
header.innerHTML = data[2].name

})


// document refers to html file connected to our JS file. One of the methods that can be used here is querySelector(), which allows us to "get" a specific html elements (only one at a time)
// let header = document.querySelector("header")
// // console.dir(header)

// // reassign the value of the HTML code inside of header to a new value
// header.innerHTML = "<p> Hello Word! </p>"

// console.log(header)