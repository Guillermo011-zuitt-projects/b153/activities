let params = new URLSearchParams(window.location.search)

let courseId = params.get("courseId")
// console.log(courseId)

/*
ACTIVIY:
Using fetch, show the details of the page's specific course in the console.
*/

fetch("http://localhost:4000/courses/" + courseId)
.then(res => res.json())
.then(data => {
console.log(data)

})