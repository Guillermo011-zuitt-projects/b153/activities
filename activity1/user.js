const User = require("../models/user")

module.exports.checkEmail = (body) => {
// the mongoDB find() method always returns an array
return User.find({email: body.email}).then(result => {
	if(result.length > 0){ // if a duplicate email is found, result.length is 1.
			return true; // true means "yes, email exists"
	}else{
			return false; // false means "no, email does not exist"
	}
})
}

// Activity s28

module.exports.register = (body) => {
	let newUser = new User({
		firstName: body.firstName,
		lastName: body.lastName,
		email: body.email,
		password: body.password,
		mobileNo: body.mobileNo
	})

return newUser.save().then((user, error) => {
		if (error){
		return false;
	}else{
		return true;
	}
})
}

