// route is use to handle request and response

// setup dependencies
const express = require("express")
const router = express.Router()
// import the controller file
const courseController = require("../controllers/course");
const auth = require("../auth")

// route to get all courses
router.get("/", (req, res) => {
	courseController.getCourses().then(resultFromController => res.send(resultFromController))
})

// "wild card" is called to get a data like a variable (ex."/:courseId")
// route to get specific course
router.get("/:courseId", (req, res) => {
	courseController.getSpecific(req.params.courseId).then(resultFromController => res.send(resultFromController))
})


// route to create a new course
// when a use sends a specific method to a specific endpoint (in this case a POST request to our /courses endpoint) the cod within this route will be run
router.post("/", auth.verify, (req, res) => {
	// auth.verify here is something called "middleware", "middleware" is any function tht must first be resolved before any succeeding code will be executed
	// show in the console the request body
	// console.log(req.body)


/*
mini activity - change the code below to restrict course creation to only admins. If a user is not an admin, send a response of false
*/

if (auth.decode(req.headers.authorization).isAdmin === true){
courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController))
}else{
	return res.send(false)
}
})

// route to update a single course
router.put("/:courseId", auth.verify, (req, res) => {
	courseController.updateCourse(req.params.courseId, req.body).then(resultFromController => res.send(resultFromController))
})

/*
Activity2:
Create a route and a controller for disabling/archiving a course with the following specification:
1. Include middleware to very the user's token
2. Restrict access to this route to ONLY admin users
3. Name the controller function "archiveCourse"
4. Return true if successful, false if not

HINT: How do we archive courses?
*/
router.delete("/:courseId", auth.verify, (req, res) => {
	courseController.archiveCourse(req.params.courseId, req.body).then(resultFromController => res.send(resultFromController))
})
	

// export the router
module.exports = router;