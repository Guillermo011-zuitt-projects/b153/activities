const Course = require("../models/course")

module.exports.getCourses = () =>{
	// find all courses then put the result in a variable called result and return that result
	return Course.find({}).then(result => {
		return result;
	})
}

module.exports.getSpecific = (courseId) =>{
	// findByID() is like findOne() except it can only find by ID
	return Course.findById(courseId).then(result => {
		return result;
	})
}

module.exports.addCourse = (body) => {

	// console.log(body)

	// create a new object called newCourse based on our Course Model. Each of it's field values will come from the request body
	let newCourse = new Course({
		name: body.name,
		description: body.description,
		price: body.price
	})


// use .save() our new newCourse object to our database. If saving is not successful, an error message will be contained inside of the error parameter passed to .then()
// if the error parameter has a value, then it will render true in our if statement, and we can send the error message as a response
// if saving is successful, the error parameter will be empty, and thus render false. This will cause our else statement to be run instead and we can confirm successful course creation as our response
return newCourse.save().then((course, error) => {
	if (error){
		return false;
	}else{
		return true;
	}
})

}

module.exports.updateCourse = (courseId, body) => {
	// create an object containing our updated course information
	let updatedCourse = {
		name: body.name,
		description: body.description,
		price: body.price
	}
	// use findByIdAndUpdate to find the course we want to update and pass the updated object as our new course data
	return Course.findByIdAndUpdate(courseId, updatedCourse).then((course, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})
}

// Activity2

module.exports.archiveCourse = (courseId, body) => {
	// create an object containing our updated course information
	let archiveCourse = {
		isActive: body.isActive
	}
	// use findByIdAndUpdate to find the course we want to update and pass the updated object as our new course data
	return Course.findByIdAndUpdate(courseId, archiveCourse).then((course, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})
}

