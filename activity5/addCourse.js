/*
Activity: Finish the add course functionality for our app. Use the following as instructions:
1. Get the #createCourse element for the HTML file via querySelector.
2. Make sure that the form's default behavior is prevented.
3. Get the input values for #courseName, #courseDescription, and #coursePrice.
4. Use a POST request to send the input's values to the /courses endpoint.
5. Make sure that you include the proper authorization header and it's value.
6. If successful, redirect the user to /courses.html. if not, show an error alert.
*/

let form = document.querySelector("#createCourse")

	form.addEventListener("submit", (e) => {
		e.preventDefault()

	let courseName = document.querySelector("#courseName").value
	let courseDescription = document.querySelector("#courseDescription").value
	let coursePrice = document.querySelector("#coursePrice").value

		fetch("http://localhost:4000/courses", {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
			name: courseName,
			description: courseDescription,
			price: coursePrice,			
	})
		})
		.then(res => res.json())
		.then(data => {
			if (data === true){
				alert("You added a new course.")
					window.location.replace("./courses.html")
			}else{
					alert("Failed to add Course. Please try again.")
				}
			})
	})





