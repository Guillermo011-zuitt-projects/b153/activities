/*
ACTIVITY:
Finish the delete/disable course feature.
1. Make sure to get the course's ID from the URL
2. Make sure to send the request to the proper endpoint with the proper method
3. Make sure all necessary headers are included
4. Do we need a body for this request, or not?
5. Upon successful respones, redirect the user to the courses page. If unsuccessful, show an error.
*/


let params = new URLSearchParams(window.location.search)

let courseId = params.get("courseId")

			fetch(`http://localhost:4000/courses/${courseId}`, {
				method: "DELETE",
				headers: {
					 Authorization: `Bearer ${localStorage.getItem("token")}`
				}
			})
			.then(res => res.json())
			.then(data => {
				if(data){
					alert("Course deleted.")
					window.location	.replace("./courses.html")
				}else{
					alert("Something went wrong. Please try again.")
				}
			})
